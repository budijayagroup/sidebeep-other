﻿using System;
using System.Collections.Generic;
using System.Transactions;
using OtherServices.Repositories;
using Sidebeep.Data;

namespace OtherServices.Services
{
    public interface IRegionService
    {
        Region FindById(int id);
        List<Region> Get();
        Region CreateRegion(Region entity);
        bool UpdateRegion(Region entity);
        bool DeleteRegion(Region entity);
    }

    public class RegionService : IRegionService
    {
        private RegionRepository _regionRepository;

        public RegionService()
        {
            _regionRepository = new RegionRepository();
        }

        public Region FindById(int id)
        {
            try
            {
                return _regionRepository.FindById(id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        
        public List<Region> Get()
        {
            try
            {
                return _regionRepository.Get();
            }
            catch (Exception ex)
            {
                return new List<Region>();
            }
        }

        public Region CreateRegion(Region entity)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    entity = _regionRepository.Create(entity);

                    transaction.Complete();
                }

                return entity;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateRegion(Region entity)
        {
            return _regionRepository.Update(entity);
        }

        public bool DeleteRegion(Region entity)
        {
            return _regionRepository.Delete(entity);
        }
    }
}
