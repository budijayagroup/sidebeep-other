﻿using System;
using System.Collections.Generic;
using System.Transactions;
using OtherServices.Repositories;
using Sidebeep.Data;

namespace OtherServices.Services
{
    public interface ISkillService
    {
        Skill FindById(int id);
        Skill FindByName(string Name);
        List<Skill> Get();
        Skill CreateSkill(Skill entity);
        bool UpdateSkill(Skill entity);
        bool DeleteSkill(Skill entity);
    }

    public class SkillService : ISkillService
    {
        private SkillRepository _skillRepository;

        public SkillService()
        {
            _skillRepository = new SkillRepository();
        }

        public Skill FindById(int id)
        {
            try
            {
                return _skillRepository.FindById(id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Skill FindByName(string Name)
        {
            try
            {
                return _skillRepository.FindByName(Name);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Skill> Get()
        {
            try
            {
                return _skillRepository.Get();
            }
            catch (Exception ex)
            {
                return new List<Skill>();
            }
        }

        public Skill CreateSkill(Skill entity)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    entity = _skillRepository.Create(entity);

                    transaction.Complete();
                }

                return entity;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateSkill(Skill entity)
        {
            return _skillRepository.Update(entity);
        }

        public bool DeleteSkill(Skill entity)
        {
            return _skillRepository.Delete(entity);
        }
    }
}
