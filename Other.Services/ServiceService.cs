﻿using System;
using System.Collections.Generic;
using System.Transactions;
using OtherServices.Repositories;
using Sidebeep.Data;

namespace OtherServices.Services
{
    public interface IServiceService
    {
        Service FindById(int id);
        List<Service> Get();
        Service CreateService(Service entity);
        bool UpdateService(Service entity);
        bool DeleteService(Service entity);
    }

    public class ServiceService : IServiceService
    {
        private ServiceRepository _serviceRepository;

        public ServiceService()
        {
            _serviceRepository = new ServiceRepository();
        }

        public Service FindById(int id)
        {
            try
            {
                return _serviceRepository.FindById(id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public Service FindByTokenAccess(string TokenAccess)
        {
            try
            {
                return _serviceRepository.FindByTokenAccess(TokenAccess);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<Service> Get()
        {
            try
            {
                return _serviceRepository.Get();
            }
            catch (Exception ex)
            {
                return new List<Service>();
            }
        }

        public Service CreateService(Service entity)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    entity = _serviceRepository.Create(entity);

                    transaction.Complete();
                }

                return entity;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateService(Service entity)
        {
            return _serviceRepository.Update(entity);
        }

        public bool DeleteService(Service entity)
        {
            return _serviceRepository.Delete(entity);
        }
    }
}
