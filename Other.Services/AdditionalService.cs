﻿using System;
using System.Collections.Generic;
using System.Transactions;
using OtherServices.Repositories;
using Sidebeep.Data;

namespace OtherServices.Services
{
    public interface IAdditionalService
    {
        Additional FindById(int id);
        Additional FindByService(int serviceId);
        List<Additional> Get();
        Additional CreateAdditional(Additional entity);
        bool UpdateAdditional(Additional entity);
        bool DeleteAdditional(Additional entity);
    }

    public class AdditionalService : IAdditionalService
    {
        private AdditionalRepository _reviewRepository;

        public AdditionalService()
        {
            _reviewRepository = new AdditionalRepository();
        }

        public Additional FindById(int id)
        {
            try
            {
                return _reviewRepository.FindById(id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Additional FindByService(int serviceId)
        {
            try
            {
                return _reviewRepository.FindByService(serviceId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Additional> Get()
        {
            try
            {
                return _reviewRepository.Get();
            }
            catch (Exception ex)
            {
                return new List<Additional>();
            }
        }

        public Additional CreateAdditional(Additional entity)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    entity = _reviewRepository.Create(entity);

                    transaction.Complete();
                }

                return entity;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateAdditional(Additional entity)
        {
            return _reviewRepository.Update(entity);
        }

        public bool DeleteAdditional(Additional entity)
        {
            return _reviewRepository.Delete(entity);
        }
    }
}
