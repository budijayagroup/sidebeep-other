﻿using System;
using System.Collections.Generic;
using System.Transactions;
using OtherServices.Repositories;
using Sidebeep.Data;

namespace OtherServices.Services
{
    public interface IPortfolioService
    {
        Portfolio FindById(int id);
        List<Portfolio> Get();
        Portfolio CreatePortfolio(Portfolio entity);
        bool UpdatePortfolio(Portfolio entity);
        bool DeletePortfolio(Portfolio entity);
    }

    public class PortfolioService : IPortfolioService
    {
        private PortfolioRepository _reviewRepository;

        public PortfolioService()
        {
            _reviewRepository = new PortfolioRepository();
        }

        public Portfolio FindById(int id)
        {
            try
            {
                return _reviewRepository.FindById(id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Portfolio> Get()
        {
            try
            {
                return _reviewRepository.Get();
            }
            catch (Exception ex)
            {
                return new List<Portfolio>();
            }
        }

        public Portfolio CreatePortfolio(Portfolio entity)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    entity = _reviewRepository.Create(entity);

                    transaction.Complete();
                }

                return entity;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdatePortfolio(Portfolio entity)
        {
            return _reviewRepository.Update(entity);
        }

        public bool DeletePortfolio(Portfolio entity)
        {
            return _reviewRepository.Delete(entity);
        }
    }
}
