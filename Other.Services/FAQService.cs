﻿using System;
using System.Collections.Generic;
using System.Transactions;
using OtherServices.Repositories;
using Sidebeep.Data;

namespace OtherServices.Services
{
    public interface IFAQService
    {
        FAQ FindById(int id);
        FAQ FindByService(int serviceId);
        List<FAQ> Get();
        FAQ CreateFAQ(FAQ entity);
        bool UpdateFAQ(FAQ entity);
        bool DeleteFAQ(FAQ entity);
    }

    public class FAQService : IFAQService
    {
        private FAQRepository _FAQRepository;

        public FAQService()
        {
            _FAQRepository = new FAQRepository();
        }

        public FAQ FindById(int id)
        {
            try
            {
                return _FAQRepository.FindById(id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public FAQ FindByService(int serviceId)
        {
            try
            {
                return _FAQRepository.FindByService(serviceId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<FAQ> Get()
        {
            try
            {
                return _FAQRepository.Get();
            }
            catch (Exception ex)
            {
                return new List<FAQ>();
            }
        }

        public FAQ CreateFAQ(FAQ entity)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    entity = _FAQRepository.Create(entity);

                    transaction.Complete();
                }

                return entity;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateFAQ(FAQ entity)
        {
            return _FAQRepository.Update(entity);
        }

        public bool DeleteFAQ(FAQ entity)
        {
            return _FAQRepository.Delete(entity);
        }
    }
}
