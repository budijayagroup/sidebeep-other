﻿using System;
using System.Collections.Generic;
using System.Transactions;
using OtherServices.Repositories;
using Sidebeep.Data;

namespace OtherServices.Services
{
    public interface IReviewService
    {
        Review FindById(int id);
        List<Review> Get();
        Review CreateReview(Review entity);
        bool UpdateReview(Review entity);
        bool DeleteReview(Review entity);
    }

    public class ReviewService : IReviewService
    {
        private ReviewRepository _reviewRepository;

        public ReviewService()
        {
            _reviewRepository = new ReviewRepository();
        }

        public Review FindById(int id)
        {
            try
            {
                return _reviewRepository.FindById(id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Review> Get()
        {
            try
            {
                return _reviewRepository.Get();
            }
            catch (Exception ex)
            {
                return new List<Review>();
            }
        }

        public Review CreateReview(Review entity)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    entity = _reviewRepository.Create(entity);

                    transaction.Complete();
                }

                return entity;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateReview(Review entity)
        {
            return _reviewRepository.Update(entity);
        }

        public bool DeleteReview(Review entity)
        {
            return _reviewRepository.Delete(entity);
        }
    }
}
