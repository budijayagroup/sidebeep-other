﻿using System;
using System.Collections.Generic;
using System.Transactions;
using OtherServices.Repositories;
using Sidebeep.Data;

namespace OtherServices.Services
{
    public interface IScheduleService
    {
        Schedule FindById(int id);
        List<Schedule> Get();
        Schedule CreateSchedule(Schedule entity);
        bool UpdateSchedule(Schedule entity);
        bool DeleteSchedule(Schedule entity);
    }

    public class ScheduleService : IScheduleService
    {
        private ScheduleRepository _scheduleRepository;

        public ScheduleService()
        {
            _scheduleRepository = new ScheduleRepository();
        }

        public Schedule FindById(int id)
        {
            try
            {
                return _scheduleRepository.FindById(id);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Schedule> Get()
        {
            try
            {
                return _scheduleRepository.Get();
            }
            catch (Exception ex)
            {
                return new List<Schedule>();
            }
        }

        public Schedule CreateSchedule(Schedule entity)
        {
            try
            {
                using (var transaction = new TransactionScope())
                {
                    entity = _scheduleRepository.Create(entity);

                    transaction.Complete();
                }

                return entity;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool UpdateSchedule(Schedule entity)
        {
            return _scheduleRepository.Update(entity);
        }

        public bool DeleteSchedule(Schedule entity)
        {
            return _scheduleRepository.Delete(entity);
        }
    }
}
