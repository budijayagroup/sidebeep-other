﻿using Sidebeep.Utils;
using System.Net;
using System.Web.Http;
using PortfolioServices.Api.Models;
using OtherServices.Services;
using System;
using Sidebeep.Data;

namespace PortfolioServices.Api.Controllers.Api
{
    public class PortfolioController : ApiController
    {
        public PortfolioService _portfolioService;

        private string errorMessage = "";

        public PortfolioController()
        {
            _portfolioService = new PortfolioService();
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult FindById(FindPortfolioIdViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _portfolioService.FindById(model.Id);

            if (ds == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "No skill found!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Get(GetPortfolioViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _portfolioService.Get();

            if (ds.Count < 1)
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "No data!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Create(ManagePortfolioViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var skill = _portfolioService.FindById(model.Id);

            if (skill != null)
            {
                apiResponse.status = (int)HttpStatusCode.MultipleChoices;
                apiResponse.message = "Skill already registered!";
                return Json(apiResponse);
            }
            else
            {
                var newPortfolio = new Portfolio();

                newPortfolio.CreatedDate = DateTime.Now;
                newPortfolio.UpdatedDate = DateTime.Now;
                newPortfolio.ServiceId = model.ServiceId;
                newPortfolio.Title = model.Title;
                newPortfolio.ImageUrl = model.ImageUrl;
                newPortfolio.Description = model.Description;

                var result = _portfolioService.CreatePortfolio(newPortfolio);

                if (result == null)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Update(ManagePortfolioViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var skill = _portfolioService.FindById(model.Id);

            if (skill == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "Portfolio not found!";
                return Json(apiResponse);
            }
            else
            {
                skill.UpdatedDate = DateTime.Now;
                skill.ServiceId = model.ServiceId;
                skill.Title = model.Title;
                skill.ImageUrl = model.ImageUrl;
                skill.Description = model.Description;

                var result = _portfolioService.UpdatePortfolio(skill);

                if (!result)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
        }
    }
}
