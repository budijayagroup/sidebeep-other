﻿using Sidebeep.Utils;
using System.Net;
using System.Web.Http;
using ReviewServices.Api.Models;
using OtherServices.Services;
using System;
using Sidebeep.Data;

namespace ReviewServices.Api.Controllers.Api
{
    public class ReviewController : ApiController
    {
        public ReviewService _reviewService;

        private string errorMessage = "";

        public ReviewController()
        {
            _reviewService = new ReviewService();
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult FindById(FindReviewIdViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _reviewService.FindById(model.Id);

            if (ds == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "No skill found!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Get(GetReviewViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _reviewService.Get();

            if (ds.Count < 1)
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "No data!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Create(ManageReviewViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var skill = _reviewService.FindById(model.Id);

            if (skill != null)
            {
                apiResponse.status = (int)HttpStatusCode.MultipleChoices;
                apiResponse.message = "Name already registered!";
                return Json(apiResponse);
            }
            else
            {
                var newReview = new Review();

                newReview.CreatedDate = DateTime.Now;
                newReview.UpdatedDate = DateTime.Now;
                newReview.ServiceId = model.ServiceId;
                newReview.BeeperId = model.BeeperId;
                newReview.Description = model.Description;

                var result = _reviewService.CreateReview(newReview);

                if (result == null)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Update(ManageReviewViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var skill = _reviewService.FindById(model.Id);

            if (skill == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "Review not found!";
                return Json(apiResponse);
            }
            else
            {
                skill.UpdatedDate = DateTime.Now;
                skill.ServiceId = model.ServiceId;
                skill.BeeperId = model.BeeperId;
                skill.Description = model.Description;

                var result = _reviewService.UpdateReview(skill);

                if (!result)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
        }
    }
}
