﻿using Sidebeep.Utils;
using System.Net;
using System.Web.Http;
using AdditionalServices.Api.Models;
using OtherServices.Services;
using System;
using Sidebeep.Data;

namespace AdditionalServices.Api.Controllers.Api
{
    public class AdditionalController : ApiController
    {
        public AdditionalService _additionalService;

        private string errorMessage = "";

        public AdditionalController()
        {
            _additionalService = new AdditionalService();
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult FindById(FindAdditionalViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _additionalService.FindById(model.Id);

            if (ds == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "No additional found!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult FindByService(FindAdditionalViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _additionalService.FindByService(model.ServiceId);

            if (ds == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "No service found!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Get(GetAdditionalViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _additionalService.Get();

            if (ds.Count < 1)
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "No data!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Create(ManageAdditionalViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var additional = _additionalService.FindById(model.Id);

            if (additional != null)
            {
                apiResponse.status = (int)HttpStatusCode.MultipleChoices;
                apiResponse.message = "Name already registered!";
                return Json(apiResponse);
            }
            else
            {
                var newAdditional = new Additional();

                newAdditional.CreatedDate = DateTime.Now;
                newAdditional.UpdatedDate = DateTime.Now;
                newAdditional.ServiceId = model.ServiceId;
                newAdditional.Name = model.Name;
                newAdditional.Price = model.Price;
                newAdditional.Qty = model.Qty;
                newAdditional.Unit = model.Unit;
                newAdditional.Description = model.Description;

                var result = _additionalService.CreateAdditional(newAdditional);

                if (result == null)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Update(ManageAdditionalViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var additional = _additionalService.FindById(model.Id);

            if (additional == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "Additional not found!";
                return Json(apiResponse);
            }
            else
            {
                additional.UpdatedDate = DateTime.Now;
                additional.ServiceId = model.ServiceId;
                additional.Name = model.Name;
                additional.Price = model.Price;
                additional.Qty = model.Qty;
                additional.Unit = model.Unit;
                additional.Description = model.Description;

                var result = _additionalService.UpdateAdditional(additional);

                if (!result)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
        }
    }
}
