﻿using Sidebeep.Utils;
using System.Net;
using System.Web.Http;
using SkillServices.Api.Models;
using OtherServices.Services;
using System;
using Sidebeep.Data;

namespace SkillServices.Api.Controllers.Api
{
    public class SkillController : ApiController
    {
        public SkillService _skillService;

        private string errorMessage = "";

        public SkillController()
        {
            _skillService = new SkillService();
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult FindById(FindSkillIdViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _skillService.FindById(model.Id);

            if (ds == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "No skill found!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult FindByName(FindSkillNameViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _skillService.FindByName(model.Name);

            if (ds == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "No skill found!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Get(GetSkillViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _skillService.Get();

            if (ds.Count < 1)
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "No data!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Create(ManageSkillViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var skill = _skillService.FindByName(model.Name);

            if (skill != null)
            {
                apiResponse.status = (int)HttpStatusCode.MultipleChoices;
                apiResponse.message = "Name already registered!";
                return Json(apiResponse);
            }
            else
            {
                var newSkill = new Skill();

                newSkill.CreatedDate = DateTime.Now;
                newSkill.UpdatedDate = DateTime.Now;
                newSkill.Name = model.Name;
                newSkill.Description = model.Description;

                var result = _skillService.CreateSkill(newSkill);

                if (result == null)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Update(ManageSkillViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var skill = _skillService.FindById(model.Id);

            if (skill == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "Skill not found!";
                return Json(apiResponse);
            }
            else
            {
                skill.UpdatedDate = DateTime.Now;
                skill.Name = model.Name;
                skill.Description = model.Description;

                var result = _skillService.UpdateSkill(skill);

                if (!result)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
        }
    }
}
