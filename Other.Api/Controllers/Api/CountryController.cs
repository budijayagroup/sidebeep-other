﻿using Sidebeep.Utils;
using System.Net;
using System.Web.Http;
using RegionServices.Api.Models;
using OtherServices.Services;
using System;
using Sidebeep.Data;

namespace RegionServices.Api.Controllers.Api
{
    public class RegionController : ApiController
    {
        public RegionService _additionalService;

        private string errorMessage = "";

        public RegionController()
        {
            _additionalService = new RegionService();
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult FindById(FindRegionViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _additionalService.FindById(model.Id);

            if (ds == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "No Service found!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Get(GetRegionViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _additionalService.Get();

            if (ds.Count < 1)
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "No data!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Create(ManageRegionViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var additional = _additionalService.FindById(model.Id);

            if (additional != null)
            {
                apiResponse.status = (int)HttpStatusCode.MultipleChoices;
                apiResponse.message = "Region already registered!";
                return Json(apiResponse);
            }
            else
            {
                var newRegion = new Region();

                newRegion.CreatedDate = DateTime.Now;
                newRegion.UpdatedDate = DateTime.Now;
                newRegion.Name = model.Name;
                newRegion.City = model.City;
                newRegion.Status = model.Status;
                newRegion.CountryId = model.CountryId;
                newRegion.TaxStatus = model.TaxStatus;
                newRegion.MarketingTools = model.MarketingTools;
                newRegion.PaymentMethods = model.PaymentMethods;

                var result = _additionalService.CreateRegion(newRegion);

                if (result == null)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Update(ManageRegionViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var additional = _additionalService.FindById(model.Id);

            if (additional == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "Region not found!";
                return Json(apiResponse);
            }
            else
            {
                additional.UpdatedDate = DateTime.Now;
                additional.Name = model.Name;
                additional.City = model.City;
                additional.Status = model.Status;
                additional.CountryId = model.CountryId;
                additional.TaxStatus = model.TaxStatus;
                additional.MarketingTools = model.MarketingTools;
                additional.PaymentMethods = model.PaymentMethods;

                var result = _additionalService.UpdateRegion(additional);

                if (!result)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
        }
    }
}
