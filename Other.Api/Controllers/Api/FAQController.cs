﻿using Sidebeep.Utils;
using System.Net;
using System.Web.Http;
using FAQServices.Api.Models;
using OtherServices.Services;
using System;
using Sidebeep.Data;

namespace FAQServices.Api.Controllers.Api
{
    public class FAQController : ApiController
    {
        public FAQService _FAQService;

        private string errorMessage = "";

        public FAQController()
        {
            _FAQService = new FAQService();
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult FindById(FindFAQIdViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _FAQService.FindById(model.Id);

            if (ds == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "No FAQ found!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult FindByService(FindFAQIdViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _FAQService.FindByService(model.ServiceId);

            if (ds == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "No service found!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Get(GetFAQViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _FAQService.Get();

            if (ds.Count < 1)
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "No data!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Create(ManageFAQViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var skill = _FAQService.FindById(model.Id);

            if (skill != null)
            {
                apiResponse.status = (int)HttpStatusCode.MultipleChoices;
                apiResponse.message = "FAQ already registered!";
                return Json(apiResponse);
            }
            else
            {
                var newFAQ = new FAQ();

                newFAQ.CreatedDate = DateTime.Now;
                newFAQ.UpdatedDate = DateTime.Now;
                newFAQ.ServiceId = model.ServiceId;
                newFAQ.FaqParentId = model.FaqParentId;
                newFAQ.FaqChildId = model.FaqChildId;
                newFAQ.SiderId = model.SiderId;
                newFAQ.BeeperId = model.BeeperId;
                newFAQ.Description = model.Description;

                var result = _FAQService.CreateFAQ(newFAQ);

                if (result == null)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Update(ManageFAQViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var skill = _FAQService.FindById(model.Id);

            if (skill == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "FAQ not found!";
                return Json(apiResponse);
            }
            else
            {
                skill.UpdatedDate = DateTime.Now;
                skill.ServiceId = model.ServiceId;
                skill.FaqParentId = model.FaqParentId;
                skill.FaqChildId = model.FaqChildId;
                skill.SiderId = model.SiderId;
                skill.BeeperId = model.BeeperId;
                skill.Description = model.Description;

                var result = _FAQService.UpdateFAQ(skill);

                if (!result)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
        }
    }
}
