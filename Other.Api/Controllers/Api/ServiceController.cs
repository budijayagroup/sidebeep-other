﻿using Sidebeep.Utils;
using System.Net;
using System.Web.Http;
using ServiceServices.Api.Models;
using OtherServices.Services;
using System;
using Sidebeep.Data;

namespace ServiceServices.Api.Controllers.Api
{
    public class ServiceController : ApiController
    {
        public ServiceService _serviceService;
        public SkillService _skillService;

        private string errorMessage = "";

        public ServiceController()
        {
            _serviceService = new ServiceService();
            _skillService = new SkillService();
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult FindById(FindServiceIdViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _serviceService.FindById(model.Id);

            if (ds == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "No service found!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult FindByTokenAccess(FindServiceIdViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _serviceService.FindByTokenAccess(model.TokenAccess);

            if (ds == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "No Sider found!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Get(GetServiceViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _serviceService.Get();

            if (ds.Count < 1)
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "No data!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Create(ManageServiceViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var skill = _skillService.FindById(model.SkillId);

            if (skill == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "Skill not found!";
                return Json(apiResponse);
            }
            else
            {
                var newService = new Service();

                newService.CreatedDate = DateTime.Now;
                newService.UpdatedDate = DateTime.Now;
                newService.Tagline = model.Tagline;
                newService.SkillId = model.SkillId;
                newService.RegionId = model.RegionId;
                newService.TokenAccess = model.TokenAccess;
                newService.Qty = model.Qty;
                newService.Unit = model.Unit;
                newService.Price = model.Price;
                newService.Title = model.Title;
                newService.Description = model.Description;

                var result = _serviceService.CreateService(newService);

                if (result == null)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    apiResponse.result = result.Id;
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Update(ManageServiceViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var service = _serviceService.FindById(model.Id);
            var skill = _skillService.FindById(model.SkillId);
            if (service == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "Service not found!";
                return Json(apiResponse);
            }

            if (skill == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "Skill not found!";
                return Json(apiResponse);
            }

            else
            {
                service.UpdatedDate = DateTime.Now;
                service.Tagline = model.Tagline;
                service.SkillId = model.SkillId;
                service.RegionId = model.RegionId;
                service.TokenAccess = model.TokenAccess;
                service.Qty = model.Qty;
                service.Unit = model.Unit;
                service.Price = model.Price;
                service.Title = model.Title;
                service.Description = model.Description;

                var result = _serviceService.UpdateService(service);

                if (!result)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
        }
    }
}
