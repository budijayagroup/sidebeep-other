﻿using Sidebeep.Utils;
using System.Net;
using System.Web.Http;
using ScheduleServices.Api.Models;
using OtherServices.Services;
using System;
using Sidebeep.Data;

namespace ScheduleServices.Api.Controllers.Api
{
    public class ScheduleController : ApiController
    {
        public ScheduleService _scheduleService;

        private string errorMessage = "";

        public ScheduleController()
        {
            _scheduleService = new ScheduleService();
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult FindById(FindScheduleIdViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _scheduleService.FindById(model.Id);

            if (ds == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "No Schedule found!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Get(GetScheduleViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var ds = _scheduleService.Get();

            if (ds.Count < 1)
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "No data!";
                return Json(apiResponse);
            }
            else
            {
                apiResponse.status = (int)HttpStatusCode.OK;
                apiResponse.message = "Data fetch successfully!";
                apiResponse.result = ds;
                return Json(apiResponse);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Create(ManageScheduleViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var skill = _scheduleService.FindById(model.Id);

            if (skill != null)
            {
                apiResponse.status = (int)HttpStatusCode.MultipleChoices;
                apiResponse.message = "Schedule already registered!";
                return Json(apiResponse);
            }
            else
            {
                var newSchedule = new Schedule();

                newSchedule.CreatedDate = DateTime.Now;
                newSchedule.UpdatedDate = DateTime.Now;
                newSchedule.ServiceId = model.ServiceId;
                newSchedule.Day = model.Day;
                newSchedule.From = model.From;
                newSchedule.To = model.To;

                var result = _scheduleService.CreateSchedule(newSchedule);

                if (result == null)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
        }
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult Update(ManageScheduleViewModel model)
        {
            ApiResponse apiResponse = new ApiResponse();

            if (!model.Validate(out errorMessage))
            {
                apiResponse.status = (int)HttpStatusCode.Unauthorized;
                apiResponse.message = errorMessage;

                return Json(apiResponse);
            }

            var skill = _scheduleService.FindById(model.Id);

            if (skill == null)
            {
                apiResponse.status = (int)HttpStatusCode.NotFound;
                apiResponse.message = "Schedule not found!";
                return Json(apiResponse);
            }
            else
            {
                skill.UpdatedDate = DateTime.Now;
                skill.ServiceId = model.ServiceId;
                skill.Day = model.Day;
                skill.From = model.From;
                skill.To = model.To;

                var result = _scheduleService.UpdateSchedule(skill);

                if (!result)
                {
                    apiResponse.status = (int)HttpStatusCode.InternalServerError;
                    apiResponse.message = "Save fail! Please try again.";
                    return Json(apiResponse);
                }
                else
                {
                    apiResponse.status = (int)HttpStatusCode.OK;
                    apiResponse.message = "Save success!";
                    return Json(apiResponse);
                }
            }
        }
    }
}
