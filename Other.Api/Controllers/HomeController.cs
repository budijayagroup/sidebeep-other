﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using OtherServices.Services;

namespace OtherServices.Api.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
