using StructureMap;
using OtherServices.Repositories;
using OtherServices.Services;

namespace OtherServices.Api {
    public static class IoC {
        private static log4net.ILog _logger = log4net.LogManager.GetLogger("Sidebeep.Skill");

        public static IContainer Initialize() {
            ObjectFactory.Initialize(x =>
                        {
                            x.Scan(scan =>
                                    {
                                        scan.TheCallingAssembly();
                                        scan.WithDefaultConventions();
                                    });

                            x.For<ISkillRepository>().Use<SkillRepository>();
                            x.For<ISkillService>().Use<SkillService>();
                        });
            return ObjectFactory.Container;
        }
    }
}