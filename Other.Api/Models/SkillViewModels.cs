﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Configuration;

namespace SkillServices.Api.Models
{
    public class FindSkillNameViewModel
    {
        public string Name { get; set; }
        public string ApiSecretKey { get; set; }

        public bool Validate(out string errorMessage)
        {
            errorMessage = "";

            if (ApiSecretKey != WebConfigurationManager.AppSettings["api-secret-key"].ToString())
            {
                errorMessage = "Invalid api secret key!";
                return false;
            }

            if (string.IsNullOrEmpty(Name))
            {
                errorMessage = "Name cannot empty!";
                return false;
            }

            return true;
        }
    }
    public class FindSkillIdViewModel
    {
        public int Id { get; set; }
        public string ApiSecretKey { get; set; }

        public bool Validate(out string errorMessage)
        {
            errorMessage = "";

            if (ApiSecretKey != WebConfigurationManager.AppSettings["api-secret-key"].ToString())
            {
                errorMessage = "Invalid api secret key!";
                return false;
            }

            if (string.IsNullOrEmpty(Id.ToString()))
            {
                errorMessage = "Id cannot empty!";
                return false;
            }

            return true;
        }
    }
    public class GetSkillViewModel
    {
        public string ApiSecretKey { get; set; }

        public bool Validate(out string errorMessage)
        {
            errorMessage = "";

            if (ApiSecretKey != WebConfigurationManager.AppSettings["api-secret-key"].ToString())
            {
                errorMessage = "Invalid api secret key!";
                return false;
            }

            return true;
        }
    }

    public class ManageSkillViewModel
    {
        public int Id{ get; set; }
        public string Name{ get; set; }
        public string Description { get; set; }
        public string ApiSecretKey { get; set; }

        public bool Validate(out string errorMessage)
        {
            errorMessage = "";

            if (ApiSecretKey != WebConfigurationManager.AppSettings["api-secret-key"].ToString())
            {
                errorMessage = "Invalid api secret key!";
                return false;
            }

            return true;
        }
    }
}