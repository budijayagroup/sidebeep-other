﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Configuration;

namespace ScheduleServices.Api.Models
{
    public class FindScheduleIdViewModel
    {
        public int Id { get; set; }
        public string ApiSecretKey { get; set; }

        public bool Validate(out string errorMessage)
        {
            errorMessage = "";

            if (ApiSecretKey != WebConfigurationManager.AppSettings["api-secret-key"].ToString())
            {
                errorMessage = "Invalid api secret key!";
                return false;
            }

            if (string.IsNullOrEmpty(Id.ToString()))
            {
                errorMessage = "Id cannot empty!";
                return false;
            }

            return true;
        }
    }
    public class GetScheduleViewModel
    {
        public string ApiSecretKey { get; set; }

        public bool Validate(out string errorMessage)
        {
            errorMessage = "";

            if (ApiSecretKey != WebConfigurationManager.AppSettings["api-secret-key"].ToString())
            {
                errorMessage = "Invalid api secret key!";
                return false;
            }

            return true;
        }
    }

    public class ManageScheduleViewModel
    {
        public int Id { get; set; }
        public int ServiceId { get; set; }
        public string Day { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string ApiSecretKey { get; set; }

        public bool Validate(out string errorMessage)
        {
            errorMessage = "";

            if (ApiSecretKey != WebConfigurationManager.AppSettings["api-secret-key"].ToString())
            {
                errorMessage = "Invalid api secret key!";
                return false;
            }

            return true;
        }
    }
}