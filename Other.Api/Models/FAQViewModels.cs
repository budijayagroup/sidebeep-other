﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Configuration;

namespace FAQServices.Api.Models
{
    public class FindFAQIdViewModel
    {
        public int Id { get; set; }
        public int ServiceId { get; set; }
        public string ApiSecretKey { get; set; }

        public bool Validate(out string errorMessage)
        {
            errorMessage = "";

            if (ApiSecretKey != WebConfigurationManager.AppSettings["api-secret-key"].ToString())
            {
                errorMessage = "Invalid api secret key!";
                return false;
            }

            if (string.IsNullOrEmpty(Id.ToString()))
            {
                errorMessage = "Id cannot empty!";
                return false;
            }

            return true;
        }
    }
    public class GetFAQViewModel
    {
        public string ApiSecretKey { get; set; }

        public bool Validate(out string errorMessage)
        {
            errorMessage = "";

            if (ApiSecretKey != WebConfigurationManager.AppSettings["api-secret-key"].ToString())
            {
                errorMessage = "Invalid api secret key!";
                return false;
            }

            return true;
        }
    }

    public class ManageFAQViewModel
    {
        public int Id { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int ServiceId { get; set; }
        public int FaqParentId { get; set; }
        public int FaqChildId { get; set; }
        public string SiderId { get; set; }
        public string BeeperId { get; set; }
        public string Description { get; set; }
        public string ApiSecretKey { get; set; }

        public bool Validate(out string errorMessage)
        {
            errorMessage = "";

            if (ApiSecretKey != WebConfigurationManager.AppSettings["api-secret-key"].ToString())
            {
                errorMessage = "Invalid api secret key!";
                return false;
            }

            return true;
        }
    }
}