﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Configuration;

namespace ServiceServices.Api.Models
{
    public class FindServiceIdViewModel
    {
        public int Id { get; set; }
        public string TokenAccess { get; set; }
        public string ApiSecretKey { get; set; }

        public bool Validate(out string errorMessage)
        {
            errorMessage = "";

            if (ApiSecretKey != WebConfigurationManager.AppSettings["api-secret-key"].ToString())
            {
                errorMessage = "Invalid api secret key!";
                return false;
            }

            if (string.IsNullOrEmpty(Id.ToString()))
            {
                errorMessage = "Id cannot empty!";
                return false;
            }

            return true;
        }
    }
    public class GetServiceViewModel
    {
        public string ApiSecretKey { get; set; }

        public bool Validate(out string errorMessage)
        {
            errorMessage = "";

            if (ApiSecretKey != WebConfigurationManager.AppSettings["api-secret-key"].ToString())
            {
                errorMessage = "Invalid api secret key!";
                return false;
            }

            return true;
        }
    }

    public class ManageServiceViewModel
    {
        public int Id { get; set; }
        public string Tagline { get; set; }
        public int SkillId { get; set; }
        public decimal Qty { get; set; }
        public string TokenAccess{ get; set; }
        public string Unit { get; set; }
        public int RegionId { get; set; }
        public decimal Price{ get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ApiSecretKey { get; set; }

        public bool Validate(out string errorMessage)
        {
            errorMessage = "";

            if (ApiSecretKey != WebConfigurationManager.AppSettings["api-secret-key"].ToString())
            {
                errorMessage = "Invalid api secret key!";
                return false;
            }

            return true;
        }
    }
}