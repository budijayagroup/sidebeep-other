﻿using Sidebeep.Data;
using Sidebeep.ORM;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace OtherServices.Repositories
{
    public interface IAdditionalRepository
    {
        Additional FindById(int id);
        Additional FindByService(int serviceId);
        List<Additional> Get();
        Additional Create(Additional entity);
        bool Update(Additional entity);
        bool Delete(Additional entity);

    }

    public class AdditionalRepository : IAdditionalRepository
    {
        private DataContext db;

        public AdditionalRepository()
        {
            db = new DataContext(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }

        public Additional FindById(int id)
        {
            var ds = db.Additional.Find(id);
            if (ds == null)
            {
                return null;
            }

            return ds;
        }
        public Additional FindByService(int serviceId)
        {
            var ds = db.Additional.FirstOrDefault(x => x.ServiceId == serviceId);
            if (ds == null)
            {
                return null;
            }

            return ds;
        }

        public List<Additional> Get()
        {
            return db.Additional.ToList();
        }

        public Additional Create(Additional entity)
        {
            entity = db.Additional.Add(entity);

            db.SaveChanges();

            return entity;
        }

        public bool Update(Additional entity)
        {
            var ds = db.Additional.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Entry(entity).CurrentValues.SetValues(ds);

            db.SaveChanges();

            return true;
        }

        public bool Delete(Additional entity)
        {
            var ds = db.Additional.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Additional.Remove(entity);

            db.SaveChanges();

            return true;
        }
    }
}
