﻿using Sidebeep.Data;
using Sidebeep.ORM;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace OtherServices.Repositories
{
    public interface ISkillRepository
    {
        Skill FindById(int id);
        Skill FindByName(string Name);
        List<Skill> Get();
        Skill Create(Skill entity);
        bool Update(Skill entity);
        bool Delete(Skill entity);

    }

    public class SkillRepository : ISkillRepository
    {
        private DataContext db;

        public SkillRepository()
        {
            db = new DataContext(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }

        public Skill FindById(int id)
        {
            var ds = db.Skill.Find(id);
            if (ds == null)
            {
                return null;
            }

            return ds;
        }
        public Skill FindByName(string Name)
        {
            var ds = db.Skill.FirstOrDefault(x => x.Name == Name);
            if (ds == null)
            {
                return null;
            }

            return ds;
        }

        public List<Skill> Get()
        {
            return db.Skill.ToList();
        }

        public Skill Create(Skill entity)
        {
            entity = db.Skill.Add(entity);

            db.SaveChanges();

            return entity;
        }

        public bool Update(Skill entity)
        {
            var ds = db.Skill.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Entry(entity).CurrentValues.SetValues(ds);

            db.SaveChanges();

            return true;
        }

        public bool Delete(Skill entity)
        {
            var ds = db.Skill.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Skill.Remove(entity);

            db.SaveChanges();

            return true;
        }
    }
}
