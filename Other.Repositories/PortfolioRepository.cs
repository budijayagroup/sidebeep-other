﻿using Sidebeep.Data;
using Sidebeep.ORM;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace OtherServices.Repositories
{
    public interface IPortfolioRepository
    {
        Portfolio FindById(int id);
        List<Portfolio> Get();
        Portfolio Create(Portfolio entity);
        bool Update(Portfolio entity);
        bool Delete(Portfolio entity);

    }

    public class PortfolioRepository : IPortfolioRepository
    {
        private DataContext db;

        public PortfolioRepository()
        {
            db = new DataContext(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }

        public Portfolio FindById(int id)
        {
            var ds = db.Portfolio.Find(id);
            if (ds == null)
            {
                return null;
            }

            return ds;
        }

        public List<Portfolio> Get()
        {
            return db.Portfolio.ToList();
        }

        public Portfolio Create(Portfolio entity)
        {
            entity = db.Portfolio.Add(entity);

            db.SaveChanges();

            return entity;
        }

        public bool Update(Portfolio entity)
        {
            var ds = db.Portfolio.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Entry(entity).CurrentValues.SetValues(ds);

            db.SaveChanges();

            return true;
        }

        public bool Delete(Portfolio entity)
        {
            var ds = db.Portfolio.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Portfolio.Remove(entity);

            db.SaveChanges();

            return true;
        }
    }
}
