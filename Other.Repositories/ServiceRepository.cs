﻿using Sidebeep.Data;
using Sidebeep.ORM;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace OtherServices.Repositories
{
    public interface IServiceRepository
    {
        Service FindById(int id);
        List<Service> Get();
        Service Create(Service entity);
        bool Update(Service entity);
        bool Delete(Service entity);

    }

    public class ServiceRepository : IServiceRepository
    {
        private DataContext db;

        public ServiceRepository()
        {
            db = new DataContext(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }

        public Service FindById(int id)
        {
            var ds = db.Service.Find(id);
            if (ds == null)
            {
                return null;
            }

            return ds;
        }
        public Service FindByTokenAccess(string TokenAccess)
        {
            var ds = db.Service.FirstOrDefault(x => x.TokenAccess == TokenAccess);
            if (ds == null)
            {
                return null;
            }

            return ds;
        }

        public List<Service> Get()
        {
            return db.Service.ToList();
        }

        public Service Create(Service entity)
        {
            entity = db.Service.Add(entity);

            db.SaveChanges();

            return entity;
        }

        public bool Update(Service entity)
        {
            var ds = db.Service.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Entry(entity).CurrentValues.SetValues(ds);

            db.SaveChanges();

            return true;
        }

        public bool Delete(Service entity)
        {
            var ds = db.Service.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Service.Remove(entity);

            db.SaveChanges();

            return true;
        }
    }
}
