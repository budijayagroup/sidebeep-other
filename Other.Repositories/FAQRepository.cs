﻿using Sidebeep.Data;
using Sidebeep.ORM;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace OtherServices.Repositories
{
    public interface IFAQRepository
    {
        FAQ FindById(int id);
        FAQ FindByService(int serviceId);
        List<FAQ> Get();
        FAQ Create(FAQ entity);
        bool Update(FAQ entity);
        bool Delete(FAQ entity);

    }

    public class FAQRepository : IFAQRepository
    {
        private DataContext db;

        public FAQRepository()
        {
            db = new DataContext(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }

        public FAQ FindById(int id)
        {
            var ds = db.FAQ.Find(id);
            if (ds == null)
            {
                return null;
            }

            return ds;
        }
        public FAQ FindByService(int serviceId)
        {
            var ds = db.FAQ.FirstOrDefault(x => x.ServiceId == serviceId);
            if (ds == null)
            {
                return null;
            }

            return ds;
        }

        public List<FAQ> Get()
        {
            return db.FAQ.ToList();
        }

        public FAQ Create(FAQ entity)
        {
            entity = db.FAQ.Add(entity);

            db.SaveChanges();

            return entity;
        }

        public bool Update(FAQ entity)
        {
            var ds = db.FAQ.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Entry(entity).CurrentValues.SetValues(ds);

            db.SaveChanges();

            return true;
        }

        public bool Delete(FAQ entity)
        {
            var ds = db.FAQ.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.FAQ.Remove(entity);

            db.SaveChanges();

            return true;
        }
    }
}
