﻿using Sidebeep.Data;
using Sidebeep.ORM;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace OtherServices.Repositories
{
    public interface IReviewRepository
    {
        Review FindById(int id);
        List<Review> Get();
        Review Create(Review entity);
        bool Update(Review entity);
        bool Delete(Review entity);

    }

    public class ReviewRepository : IReviewRepository
    {
        private DataContext db;

        public ReviewRepository()
        {
            db = new DataContext(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }

        public Review FindById(int id)
        {
            var ds = db.Review.Find(id);
            if (ds == null)
            {
                return null;
            }

            return ds;
        }

        public List<Review> Get()
        {
            return db.Review.ToList();
        }

        public Review Create(Review entity)
        {
            entity = db.Review.Add(entity);

            db.SaveChanges();

            return entity;
        }

        public bool Update(Review entity)
        {
            var ds = db.Review.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Entry(entity).CurrentValues.SetValues(ds);

            db.SaveChanges();

            return true;
        }

        public bool Delete(Review entity)
        {
            var ds = db.Review.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Review.Remove(entity);

            db.SaveChanges();

            return true;
        }
    }
}
