﻿using Sidebeep.Data;
using Sidebeep.ORM;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace OtherServices.Repositories
{
    public interface IRegionRepository
    {
        Region FindById(int id);
        List<Region> Get();
        Region Create(Region entity);
        bool Update(Region entity);
        bool Delete(Region entity);

    }

    public class RegionRepository : IRegionRepository
    {
        private DataContext db;

        public RegionRepository()
        {
            db = new DataContext(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }

        public Region FindById(int id)
        {
            var ds = db.Region.Find(id);
            if (ds == null)
            {
                return null;
            }

            return ds;
        }
        public List<Region> Get()
        {
            return db.Region.ToList();
        }

        public Region Create(Region entity)
        {
            entity = db.Region.Add(entity);

            db.SaveChanges();

            return entity;
        }

        public bool Update(Region entity)
        {
            var ds = db.Region.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Entry(entity).CurrentValues.SetValues(ds);

            db.SaveChanges();

            return true;
        }

        public bool Delete(Region entity)
        {
            var ds = db.Region.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Region.Remove(entity);

            db.SaveChanges();

            return true;
        }
    }
}
