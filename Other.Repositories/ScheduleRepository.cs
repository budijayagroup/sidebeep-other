﻿using Sidebeep.Data;
using Sidebeep.ORM;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace OtherServices.Repositories
{
    public interface IScheduleRepository
    {
        Schedule FindById(int id);
        List<Schedule> Get();
        Schedule Create(Schedule entity);
        bool Update(Schedule entity);
        bool Delete(Schedule entity);

    }

    public class ScheduleRepository : IScheduleRepository
    {
        private DataContext db;

        public ScheduleRepository()
        {
            db = new DataContext(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }

        public Schedule FindById(int id)
        {
            var ds = db.Schedule.Find(id);
            if (ds == null)
            {
                return null;
            }

            return ds;
        }

        public List<Schedule> Get()
        {
            return db.Schedule.ToList();
        }

        public Schedule Create(Schedule entity)
        {
            entity = db.Schedule.Add(entity);

            db.SaveChanges();

            return entity;
        }

        public bool Update(Schedule entity)
        {
            var ds = db.Schedule.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Entry(entity).CurrentValues.SetValues(ds);

            db.SaveChanges();

            return true;
        }

        public bool Delete(Schedule entity)
        {
            var ds = db.Schedule.Find(entity.Id);
            if (ds == null)
            {
                return false;
            }

            db.Schedule.Remove(entity);

            db.SaveChanges();

            return true;
        }
    }
}
